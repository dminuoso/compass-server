# compass-server

compass-server is a RADIUS server miniframework intended to be used with [compass-radius](https://gitlab.com/dminuoso/compass-radius).

# Motivation
I developed compass-server out of the need to have a tractable RADIUS server whose semantics could be easily verified and tested,
while having the full flexibility to interleave typical RADIUS access semantics with arbitrary code.

# RFC compliance
compass-server is *not* RFC compliant in the sense that it does not enforce or validate number/presence/length of attributes. It is up
to the user of compass-server to ensure the server remais RFC compliant. I may add verification utilities in the future to provide
combinators to enforce RFC compliance.

# EAP
There is no explicit support for EAP. The necessary RADIUS attributes exist in the serialization library, but the user will have to delegate
EAP messages to either another library/server or implement EAP themselves.
