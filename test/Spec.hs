{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeApplications  #-}

import           Data.Maybe
import           System.Exit (exitFailure, exitSuccess)

import           Compass.Authentication
import           Compass.Env
import           Compass.Handler
import           Compass.Lens
import           Compass.Server
import qualified Data.ByteString as B
import           Network.RADIUS.Types
import           Test.QuickCheck hiding ((.&&.))
import           Test.QuickCheck.Monadic

import           Optics

import           Arbitrary (nonEmptyAlpha)

pair :: Gen (B.ByteString, B.ByteString)
pair = arbitrary

mismatchingR :: Gen (Env, B.ByteString, B.ByteString)
mismatchingR = do
  actual   <- nonEmptyAlpha
  expected <- nonEmptyAlpha `suchThat` (/= actual)
  user     <- nonEmptyAlpha
  env      <- arbitrary @Env

  let full = user <> "@" <> actual
  let env' = env & pAttrs .~ [UserNameAttribute full]
  pure (env', user, expected)

matchingR :: Gen (Env, B.ByteString, B.ByteString)
matchingR = do
  realmName <- nonEmptyAlpha
  user      <- nonEmptyAlpha
  env       <- arbitrary

  let full = user <> "@" <> realmName
  let env' = env & pAttrs .~ [UserNameAttribute full]

  pure (env', user, realmName)

assertJust :: (Monad m) => Maybe Bool -> PropertyM m ()
assertJust m = assert $ fromMaybe False m

prop_realmSkips :: Property
prop_realmSkips = forAll mismatchingR $ \(env, _user, realmName) -> monadicIO $ do
  resp <- run $ runHandler (realm realmName) env
  case resp of
    Just _resp -> assert False
    Nothing    -> assert True

prop_realmAccepts :: Property
prop_realmAccepts = forAll matchingR $ \(env, user, realmName) -> monadicIO $ do
  resp <- run $ runHandler (realm realmName) env
  case resp of
    Just resp' -> assert (resp' == user)
    Nothing    -> assert False

prop_echoIsId :: Env -> Property
prop_echoIsId env@Env{..} = monadicIO $ do
  resp <- run $ runHandler echo env
  case resp of
    Just resp' -> assert (resp' == _getPacket)
    Nothing    -> assert False

runTests :: IO Bool
runTests = $quickCheckAll

main :: IO ()
main = do
  r <- runTests
  if r
    then exitSuccess
    else exitFailure
