module Arbitrary where

import Test.QuickCheck

import Data.Word8

import qualified Data.ByteString as B
import Data.IP
import Data.IP.RouteTable
import Control.Monad (replicateM)
import Control.Applicative

import Compass.Env
import Network.RADIUS.Types

asciiBS :: Gen B.ByteString
asciiBS = arbitrary `suchThat` B.all isAscii

nonEmpty :: Gen B.ByteString
nonEmpty = arbitrary `suchThat` ((> 0) . B.length)

nonEmptyAlpha :: Gen B.ByteString
nonEmptyAlpha = nonEmpty `suchThat` B.all isAlpha

instance Arbitrary B.ByteString where
  arbitrary = fmap B.pack arbitrary

instance Arbitrary Secret where
  arbitrary = Secret <$> arbitrary

instance Arbitrary IPv4 where
  arbitrary = arbitraryAdr toIPv4 255 4

instance Arbitrary IPv6 where
  arbitrary = arbitraryAdr toIPv6 65535 8

arbitraryAdr :: Routable a => ([Int] -> a) -> Int -> Int -> Gen a
arbitraryAdr func width adrlen = func <$> replicateM adrlen (choose (0, width))

instance Arbitrary IP where
  arbitrary = oneof [IPv4 <$> arbitrary, IPv6 <$> arbitrary]

instance Arbitrary Config where
  arbitrary = liftA2 Config arbitrary arbitrary

instance Arbitrary Client where
  arbitrary = liftA2 Client arbitrary arbitrary

instance Arbitrary PacketType where
  arbitrary = elements [ AccessRequest
                       , AccessAccept
                       , AccessReject
                       , AccountingRequest
                       , AccountingResponse
                       , AccessChallenge
                       , StatusServer
                       , StatusClient
                       ]

instance Arbitrary Header where
  arbitrary = Header <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary Packet where
  arbitrary = do
    header <- arbitrary
    username <- arbitrary :: Gen B.ByteString

    pure $ Packet header [UserNameAttribute username]

instance Arbitrary Env where
  arbitrary = Env <$> arbitrary <*> arbitrary <*> arbitrary
