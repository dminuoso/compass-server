{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE KindSignatures    #-}
{-# LANGUAGE OverloadedStrings #-}

module Compass.Handler
  ( Rad(..)
  , RadKind(..)
  , Immediate(..)
  , accept
  , acceptWith
  , reject
  , rejectWith
  , rejectWhy
  , acknowledge
  , acknowledgeWith
  , ignoreWhy
  , whenAccess
  , whenAccounting
  , both
  , captureImmediates
  , immediateIgnore
  , immediateReject
  )
where

-- Base
import           Control.Exception (Exception)
import           Data.Typeable (Typeable)

-- Extra
import           Data.Text (Text)
import           Network.RADIUS.Encoding ()
import           Network.RADIUS.Types (Attribute(..), Packet(..),
                                       PacketType(..))
import           Optics hiding (both)
import qualified UnliftIO.Exception as E (catch, throwIO)

-- Compass
import           Compass.Env
import           Compass.Handler.Internal
import           Compass.Types

-- | Internal function used to construct a radius response with.
answer :: PacketType -> [Attribute] -> Rad k Response
answer typ as = do
  req <- gview _requestPacket
  let pk = Packet { packetType = typ
                  , packetId = (packetId req)
                  , packetLength = 0
                  , packetAuthenticator = (packetAuthenticator req)
                  , packetAttributes = as }
  pure (Answer pk)

-- | Accept a RADIUS Access request with a given set of attributes.
acceptWith :: [Attribute] -> Rad 'Access Response
acceptWith = answer AccessAccept

-- | Accept a RADIUS Access request with no attributes.
accept :: Rad 'Access Response
accept = acceptWith []

-- | Reject a RADIUS Access request with a given set of attributes.
-- This can be useful to provide a rejection reason to the NAS.
rejectWith :: [Attribute] -> Rad 'Access Response
rejectWith = answer AccessReject

-- | reject a RADIUS Access request with no attributes.
reject :: Rad 'Access Response
reject = rejectWith []

-- | Reject a RADIUS Access request with a reply message.
-- This is a convenience function around 'rejectWith'.
rejectWhy :: Text -> Rad 'Access Response
rejectWhy why = rejectWith [AttrReplyMessage why]

-- | Acknowlege a RADIUS Accounting request with a given set of attributes.
acknowledgeWith :: [Attribute] -> Rad 'Accounting Response
acknowledgeWith = answer AccountingResponse

-- | Acknowledge a RADIUS Accounting with no attributes.
acknowledge :: Rad 'Accounting Response
acknowledge = acknowledgeWith []

-- | Disregard any RADIUS access or accounting request.
ignoreWhy :: Text -> Rad (k :: RadKind) Response
ignoreWhy why = pure (Discard why)

-- | An exception to shortcircuit the rest of the handler and provide an instant
-- response. This is mainly useful to discard or reject packages from deeply nested
-- handlers.
-- This is used over say 'ExceptT' because it lets us use 'unliftio', which is easier
-- to reason about than 'MonadBaseControl'.
data Immediate = Immediate Response
  deriving (Show, Eq, Typeable)

instance Exception Immediate

-- | Capture an 'Immediate' exception and reinsert the contained answer as a
-- result.
captureImmediates :: Rad k Response -> Rad k Response
captureImmediates app = app `E.catch` immediate
  where
    immediate :: Immediate -> Rad k Response
    immediate (Immediate response) = pure response

-- | Immediately ignore a RADIUS request, shortcircuiting the rest of the handler.
-- The exception can be caught by standard exception mechanisms or with 'captureImmediates'
immediateIgnore :: Text -> Rad k a
immediateIgnore why = E.throwIO . Immediate =<< ignoreWhy why

-- | Immediately reject an Access request, shortcircuiting the rest of the handler.
-- The exception can be caught by standard exception mechanisms or with 'captureImmediates'
immediateReject :: Text -> Rad 'Access a
immediateReject why = E.throwIO . Immediate =<< rejectWhy why
