{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Compass.Prometheus
  ( monitorServer
  , registerMeters
  , exportMetricsAsText
  )
where


import Control.Monad.IO.Class (liftIO)
import Optics
import Prometheus

import Data.Text (Text, pack)
import Network.RADIUS.Lens
import Network.RADIUS.Types
import System.Clock (Clock(..), TimeSpec(..), diffTimeSpec, getTime)
import UnliftIO.Exception (bracket, bracket_, onException)

import Compass.Env
import Compass.Handler (Rad)
import Compass.Types (Response(..))

-- | Registers the meters with the global prometheus registry.
registerMeters :: IO Meters
registerMeters = do
  let mResponses = vector ("request_type", "client_addr", "client_name", "response_type")
                          (counter (Info "radius_responses"
                                         "Counters for RADIUS responses"))
      mRequests = vector ("request_type", "client_addr", "client_name")
                         (counter (Info "radius_requests"
                                        "Counters for RADIUS requests"))
      mErrors = vector ("request_type", "client_addr", "client_name")
                       (counter (Info "compass_uncaught_exceptions"
                                      "Amount of uncaught exceptions in request handlers"))
      mInflight = vector ("request_type", "client_addr", "client_name")
                         (gauge (Info "radius_requests_inflight"
                                      "Counters for RADIUS requests currently inflight"))
      mTime = vector ("request_type", "client_addr", "client_name")
        (histogram (Info "radius_requests_duration_seconds" "Duration between RADIUS requests and responses")
                   bucket_sizes)

  metersResponses <- register mResponses
  metersRequests <- register mRequests
  metersErrors <- register mErrors
  metersInflight <- register mInflight
  metersTime <- register mTime

  pure Meters{..}

    where
      bucket_sizes = [0.001, 0.002, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0]
-- | Data record holding the prometheus metrics.
data Meters = Meters
  { metersRequests  :: Vector Label3 Counter
  , metersResponses :: Vector Label4 Counter
  , metersInflight  :: Vector Label3 Gauge
  , metersErrors    :: Vector Label3 Counter
  , metersTime      :: Vector Label3 Histogram
  }

-- | Measure total duration of requests in seconds
instrTime :: Meters -> (Text, Text, Text) -> Rad k Response -> Rad k Response
instrTime Meters{metersTime} (reqTy, caddr, cname) handler =
    bracket start stop (const handler)
  where
    start = liftIO (getTime Monotonic)
    stop startedAt = do
      stoppedAt <- liftIO (getTime Monotonic)
      let spent = totalSecs (diffTimeSpec stoppedAt startedAt)
      liftIO (withLabel metersTime (reqTy, caddr, cname) (`observe` spent))
      where
        totalSecs :: TimeSpec -> Double
        totalSecs (TimeSpec secs nanos) = fromIntegral secs + (fromIntegral nanos / (1000 * 1000 * 1000))

-- | Measure requests currently inflight
instrInflight :: Meters -> (Text, Text, Text) -> Rad k Response -> Rad k Response
instrInflight Meters{metersInflight} (reqTy, caddr, cname) handler = bracket_
  (liftIO (withLabel metersInflight (reqTy, caddr, cname) incGauge))
  (liftIO (withLabel metersInflight (reqTy, caddr, cname) decGauge))
  handler

-- | Measure responses per client
instrReq :: Meters -> (Text, Text, Text) -> Rad k Response -> Rad k Response
instrReq Meters{metersResponses, metersRequests} (reqTy, caddr, cname) handler = do
    logReq
    resp <- handler

    case resp of
      Answer pak   -> logRes (showT (packetType pak))
      Discard _why -> logRes "Discard"
      Skip         -> logRes "Skip"

    pure resp

   where
     logReq = do
       liftIO (withLabel metersRequests (reqTy, caddr, cname) incCounter)
     logRes res = do
       liftIO (withLabel metersResponses (reqTy, caddr, cname, res) incCounter)

labels :: Rad k (Text, Text, Text)
labels = do
  reqTy <- gview _packetType
  caddr <- gview _clientAddr
  cname <- gview _clientName
  pure (showT reqTy, showT caddr, cname)

showT :: Show a => a -> Text
showT = pack . show

-- | Measure exceptions that somehow escape the handler
instrEx :: Meters -> (Text, Text, Text) -> Rad k Response -> Rad k Response
instrEx Meters{metersErrors} (reqTy, caddr, cname) handler
  = handler `onException` (liftIO (withLabel metersErrors (reqTy, caddr, cname) incCounter))

-- | Inserts instrumentation into a Compass application.
monitorServer :: Meters -> Rad k Response -> Rad k Response
monitorServer meters app = do
  lab <- labels
  instrument lab app
    where
      instrument l = instrTime meters l
                   . instrInflight meters l
                   . instrReq meters l
                   . instrEx meters l
