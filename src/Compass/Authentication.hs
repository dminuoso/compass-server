{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}

module Compass.Authentication
  ( realm
  , realm'
  , realms
  , realms'
  , papPassword
  , papWithAttrs
  , simplePapScheme
  )
where

-- Base
import           Control.Exception (Exception)
import           Control.Monad.IO.Class
import           Data.Foldable (asum)
import           Optics

-- Extras
import           Control.Monad.Reader
import           Control.Monad.Catch (MonadThrow, throwM)
import qualified Data.ByteString as S
import           Data.Text as T
import           Network.RADIUS.Crypto (decodePassword)
import           Network.RADIUS.Types (Attribute)

-- Compass
import           Compass.Env
import           Compass.Handler
import           Compass.Types
import           Network.RADIUS.Lens


note :: (MonadThrow m, Exception e) => e -> Maybe a -> m a
note = flip maybe pure . throwM

noteM :: Monad m => m (Maybe a) -> m a -> m a
noteM act els = do
  r <- act
  case r of
    Nothing -> els
    Just x  -> pure x

-- | Guards if the realm does not match. If it matches, return the user portion.
-- Throws `IgnoreEx` to bail out.
realm' :: T.Text -> Rad k T.Text
realm' r = realm r >>= note (Immediate (Discard "User-Mame does not match required realm"))

gpreview :: MonadReader s0 m0 => Optic' A_Fold is s0 r -> m0 (Maybe r)
gpreview = gview . pre

realm :: T.Text -> Rad k (Maybe T.Text)
realm m = do
  user <- gpreview (_packetAttributes % folded % _AttrUserName)
  let suff = "@" <> m
  pure (user >>= T.stripSuffix suff)

realms :: [T.Text] -> Rad k (Maybe T.Text)
realms ms = do
  user <- gpreview (_packetAttributes % folded % _AttrUserName)
  pure $ do { u' <- user
            ; let suff = "@" <> u'
            ; asum (T.stripSuffix suff <$> ms)
            }

-- | Guards if none of the realms match. If they match return the user portion.
-- Throws `IgnoreEx` to bail out.
realms' :: [T.Text] -> Rad k T.Text
realms' ms = realms ms >>= note (Immediate (Discard "User-Name does not match any required realm"))

papPassword :: Rad 'Access (Maybe S.ByteString)
papPassword = do
  Secret sec <- gview _clientSecret
  auth <- gview _packetAuthenticator
  pass <- gpreview (_packetAttributes % folded % _AttrUserPassword)

  pure (pass >>= decodePassword auth sec)

papWithAttrs :: (T.Text -> S.ByteString -> IO (Maybe [Attribute])) -> Rad 'Access Response
papWithAttrs auth = do
  user <- gpreview (_packetAttributes % folded % _AttrUserName) `noteM` immediateReject "Missing username"
  pass <- papPassword `noteM` immediateReject "Missing password"
  result <- liftIO (auth user pass)
  case result of
    Nothing -> reject
    Just as -> acceptWith as

-- | Construct a simple access server using an authentication function.
simplePapScheme :: (T.Text -> S.ByteString -> IO Bool) -> Rad 'Access Response
simplePapScheme auth = do
  user <- gpreview (_packetAttributes % folded % _AttrUserName) `noteM` immediateReject "Missing username"
  pass <- papPassword `noteM` immediateReject "Missing password"
  authenticated <- liftIO (auth user pass)
  if authenticated
    then accept
    else reject
