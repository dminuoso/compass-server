{-# LANGUAGE TemplateHaskell #-}
module Compass.Env
where

import Optics
import Optics.TH
import Data.IP (IP)
import Data.Text (Text)
import Data.ByteString as S
import Control.Concurrent.MVar (MVar)
import Network.RADIUS.Types
import Network.RADIUS.Lens

newtype Secret = Secret { unSecret :: S.ByteString } deriving (Show, Eq)

data Client = Client { clientName   :: !Text
                     , clientAddr   :: !IP
                     , clientSecret :: !Secret
                     } deriving (Show, Eq)

data Request = Request
  { requestClient   :: !Client
  , requestPacket   :: !Packet
  } deriving (Show, Eq)

data Config = Config { configPort           :: Integer
                     , configListenAddress  :: IP
                     , configAllowedClients :: [Client]
                     , configStartupTok     :: MVar ()
                     , configHandlerTimeout :: Int
                     } deriving Eq

$(makeLensesFor  [ ("requestClient", "_requestClient")
                 , ("requestPacket", "_requestPacket")
                 ] ''Request)

$(makeClassyFor "HasClient" "_client"
                [ ("clientName", "_clientName")
                , ("clientAddr", "_clientAddr")
                , ("clientSecret", "_clientSecret")
                ] ''Client)

instance HasClient Request where
  _client = _requestClient

instance HasPacket Request where
  _radiusPacket = _requestPacket
