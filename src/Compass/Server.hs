{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TemplateHaskell #-}

module Compass.Server
  ( accessServer
  , accountingServer
  , runServer
  )
where

-- Base
import           Data.Void (Void)
import           Control.Monad (guard)
import           Control.Exception (Exception(..), SomeException(..))
import           Control.Concurrent.MVar (putMVar)
import           Data.List (find)
import           Data.Typeable (Typeable)
import           Data.Text (pack, Text)
import           GHC.Conc (pseq)

-- Extra
import           Hexdump (prettyHex)
import           Control.Monad.IO.Class (liftIO)
import           Control.Monad.Reader (ReaderT(..))
import           Control.Monad.Logger (LoggingT)
import           UnliftIO (withRunInIO, finally, withException, mask_, timeout)
import           UnliftIO.Async (asyncWithUnmask)
import qualified UnliftIO.Exception as E (bracket, catch, try)
import           UnliftIO.STM (atomically, TVar, readTVar, modifyTVar, newTVarIO)
import qualified Data.ByteString as S
import           Data.IP
  ( fromSockAddr
  ,  IP
  )
import           Network.RADIUS.Encoding
import           Network.RADIUS.Types
import           Network.Socket
  ( SockAddr(..)
  , Socket
  , AddrInfo(..)
  , bind
  , setSocketOption
  , SocketOption(..)
  , SocketType(..)
  , socket
  , defaultProtocol
  , AddrInfoFlag(..)
  , defaultHints
  , getAddrInfo
  , withSocketsDo
  , close
  , fdSocket
  )
import           Network.Socket.ByteString (recvFrom, sendAllTo)
import           Control.Monad.Logger

import System.Posix.IO
  ( FdOption(..)
  , setFdOption
  )

-- Compass
import           Compass.Handler.Internal
import           Compass.Handler
import           Compass.Env

import Compass.Types

data ParseError = ParseError String deriving (Eq, Show, Typeable)
instance Exception ParseError

type IOL = LoggingT IO

-- | Unlifts the argument, puts it into 'withSocketsDo' and lifts it back.
withSocketsDo' :: IOL a -> IOL a
withSocketsDo' a = withRunInIO $ \io ->
  liftIO (withSocketsDo (io a))

-- | Run an access server only. Non-access requests are skipped.
accessServer :: Config
             -> Rad 'Access Response
             -> LoggingT IO Void
accessServer c h = runServerRaw c (whenAccess h)

-- | Run an accounting server only. Non-accounting requests are skipped.
accountingServer :: Config
                 -> Rad 'Accounting Response
                 -> LoggingT IO Void
accountingServer c h = runServerRaw c (whenAccounting h)

-- | Run both an accounting and access server on the same port.
runServer :: Config
          -> Rad 'Access Response
          -> Rad 'Accounting Response
          -> LoggingT IO Void
runServer c acc act =
  runServerRaw c (both acc act)

-- | Run a raw compass server.
runServerRaw :: Config          -- ^ Config (deeply strict)
             -> RadRaw Response -- ^ The generic handler
             -> LoggingT IO Void
runServerRaw c h = nf c `pseq` do
    withSocketsDo' $ do
      addr <- resolve c
      E.bracket
          (open' addr)
          close'
          (\sock -> do
            noteStartUp
            dispatcher c h sock)
  where
    close' :: Socket -> IOL ()
    close' = liftIO . close

    open' :: AddrInfo -> IOL Socket
    open' addr = do
      sock <- open addr
      setSocketCloseOnExec sock
      pure sock

    -- Notify the server has started up by putting into the MVar.
    -- Interested parties can `readMVar` to get notified when the
    -- server is up and running.
    noteStartUp :: IOL ()
    noteStartUp = liftIO (putMVar (configStartupTok c) ())

    -- | Resolves a config to an `AddrInfo` we use to bind the server to.
    --
    resolve :: Config -> IOL AddrInfo
    resolve Config{configPort, configListenAddress} = liftIO $ do
        -- This is not an incomplete pattern match because `getAddrInfo`
        -- guarantees to return a non-empty list, it throws otherwise.
        (best:_others) <- getAddrInfo (Just hints) (Just addr) (Just port)
        pure best
      where
        addr = show configListenAddress
        port = show configPort

-- | Bring Config into a normal form. Used to ensure we are not handed
-- _|_ inside an IP address.
nf :: Config -> ()
nf !c = foldr seq () (configAllowedClients c)

setSocketCloseOnExec :: Socket -> IOL ()
setSocketCloseOnExec sock = do
  let fd = fromIntegral (fdSocket sock)
  liftIO (setFdOption fd CloseOnExec True)

hints :: AddrInfo
hints = defaultHints { addrFlags = [AI_PASSIVE]
                     , addrSocketType = Datagram
                     }

-- | Open a socket
open :: AddrInfo -> IOL Socket
open addr = do
    liftIO $ do { sock <- socket (addrFamily addr) Datagram defaultProtocol
                ; setSocketOption sock ReuseAddr 1
                ; bind sock (addrAddress addr)
                ; pure sock
                }

maxRadPkSz :: Int
maxRadPkSz = 4096

-- | Main dispatcher loop.
dispatcher :: Config
           -> RadRaw Response
           -> Socket
           -> IOL Void
dispatcher conf handler sock = do
    cnt <- liftIO (newTVarIO 0)
    loop cnt `withException` waitWorkers cnt
  where
    -- We need to ensure that when shutting down, all workers
    -- are allowed to gracefully exit.
    -- This is done by the following mechanism:
    --
    -- We increment the TVar counter by one when a thread is spawned.
    -- We decrement the TVar counter by one when a thread is finished
    --   or if it throws.
    --
    --
    -- If an async exception is thrown right after `addThread`, the
    -- counter could never reach 0. This is why we must mask until the
    -- worker has been spawned.
    --
    -- The code might consume partial data off the socket and then die, but
    -- because this is UDP the client cannot tell the difference.
    loop :: TVar Integer -> IOL Void
    loop cnt = do
      (msg, addr) <- liftIO (recvFrom sock maxRadPkSz)
      _thread <- mask_ $ do
        addThread cnt
        asyncWithUnmask $ \unmask ->
             unmask (handle addr msg) `finally` delThread cnt
      loop cnt

    handle :: SockAddr -> S.ByteString -> IOL ()
    handle a m = do
      r <- timeout (secs (configHandlerTimeout conf)) (handleRequest conf handler sock a m)
      case r of
        Just () -> pure ()
        Nothing -> logErrorN "Request handler timed out. Worker killed"

    secs :: Int -> Int
    secs x = x * 1000 * 1000

    addThread :: TVar Integer -> IOL ()
    addThread cnt = atomically (modifyTVar cnt (+1))

    delThread :: TVar Integer -> IOL ()
    delThread cnt = atomically (modifyTVar cnt (subtract 1))

    waitWorkers :: TVar Integer -> SomeException -> IOL ()
    waitWorkers cnt err = do
      logWarnN ("Exception received: " <> pack (displayException err))
      logDebugN "Waiting for workers to complete"
      atomically $ do
        c <- readTVar cnt
        guard (c == 0)

-- | Handle a request. Each request is run inside a separate thread.
handleRequest :: Config -- ^ Config
              -> RadRaw Response -- ^ Request handler
              -> Socket -- ^ Socket to respond to
              -> SockAddr -- ^ Remote addr
              -> S.ByteString -- ^ Request bytes
              -> IOL ()
handleRequest conf app conn addr msg =
    case client of
      -- Client is unknown
      Nothing -> discard addr

      -- Client is whitelisted
      Just cl -> acceptBits cl app answer msg
  where
    client :: Maybe Client
    client = do
      ip <- getAddr addr
      lookupClient ip

    -- Look for the client matching the IP address
    lookupClient :: IP -> Maybe Client
    lookupClient add = find (\cl -> clientAddr cl == add) (configAllowedClients conf)

    getAddr :: SockAddr -> Maybe IP
    getAddr = fmap fst . fromSockAddr

    -- Send some bytes back to the client
    answer :: S.ByteString -> IOL ()
    answer resp' = liftIO (sendAllTo conn resp' addr)

    discard :: SockAddr -> IOL ()
    discard add = logInfoN (discardMsg (showT add))
      where
        discardMsg cli = "Silently discarding request from unknown client: " <> cli

        showT :: Show a => a -> Text
        showT = pack . show

acceptBits :: Client
          -> RadRaw Response
          -> (S.ByteString -> IOL ())
          -> S.ByteString
          -> IOL ()
acceptBits client app answer bitsIn = do
    logOtherN (LevelOther "DebugBits") ("Incoming bits\n" <> prettyHexT bitsIn)
    case decodePacket bitsIn of
        Left err -> logWarnN ("Invalid packet received: " <> pack err)
        Right pak -> acceptPak client app answer pak

acceptPak :: Client
          -> RadRaw Response
          -> (S.ByteString -> IOL ())
          -> Packet
          -> IOL ()
acceptPak c app ans pak = acceptPakRaw c app' ans
  where
    app' :: IOL Response
    app' = runApp (catchImm app) (Request c pak)

    catchImm :: RadRaw Response -> RadRaw Response
    catchImm ap = ap `E.catch` reinjImm

    reinjImm :: Immediate -> RadRaw Response
    reinjImm (Immediate response) = pure response

acceptPakRaw :: Client
             -> IOL Response
             -> (S.ByteString -> IOL ())
             -> IOL ()
acceptPakRaw c app answer = do
      resp <- E.try app
      case resp of
          Left exc            -> uncaught exc
          Right Skip          -> logSkip
          Right (Discard why) -> logDisc why
          Right (Answer p)    -> respond p
  where
    uncaught :: SomeException -> IOL ()
    uncaught ex = logErrorN ("Uncaught exception: " <> pack (displayException ex))

    logSkip :: IOL ()
    logSkip = logWarnN "Discarding request: Handler failed to "

    logDisc :: Text -> IOL ()
    logDisc why = logInfoN ("Discarding request: " <> why)

    -- Send a package as a response.
    respond :: Packet -> IOL ()
    respond p = do
      logOtherN (LevelOther "DebugBits") ("Sending bits\n" <> prettyHexT bits)
      answer bits
        where
          bits = sign (encodePacket p) secret

    secret :: S.ByteString
    secret = unSecret (clientSecret c)

prettyHexT :: S.ByteString -> Text
prettyHexT = pack . prettyHex

-- | Run a handler. This catches any propagated 'Immediate' resposnes
-- and reinjects the contained response into the result.
runApp :: RadRaw Response -> Request -> IOL Response
runApp = runReaderT . runRaw
