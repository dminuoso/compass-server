{-# LANGUAGE DataKinds #-}
module Compass.Types
  ( RadKind(..)
  , Response(..)
  )
where

import Data.Text
import Network.RADIUS.Types

data RadKind
  = Accounting
  | Access

data Response
  -- Send a response to a client
  = Answer Packet

  -- Discard a clients request
  | Discard Text

  -- Skip this handler in hopes of another one catching.
  | Skip
  deriving (Show, Eq)
