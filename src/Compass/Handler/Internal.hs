{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures             #-}

module Compass.Handler.Internal
where

import Control.Monad.Catch
import Control.Monad.IO.Class
import Control.Monad.Logger
import Control.Monad.Reader
import Data.Coerce
import Network.RADIUS.Types
import UnliftIO (MonadUnliftIO)

import Optics

import Compass.Env
import Compass.Types
import Network.RADIUS.Lens

  -- The internal monad representation without the radius type singleton type.
newtype RadRaw s = RadRaw
  { runRaw :: ReaderT Request (LoggingT IO) s
  } deriving (Functor, Applicative, Monad)
    deriving (MonadReader Request, MonadThrow, MonadIO, MonadUnliftIO, MonadLogger)

-- | The main compass monad.
newtype Rad (k :: RadKind) s = Rad
  { runRad :: RadRaw s
  } deriving (Functor, Applicative, Monad)
    deriving (MonadReader Request, MonadThrow, MonadIO, MonadUnliftIO, MonadLogger)

whenAccess :: Rad 'Access Response
           -> RadRaw Response
whenAccess handler = do
  rTy <- gview _packetType
  if rTy == AccessRequest
    then coerce handler
    else pure Skip

whenAccounting :: Rad 'Accounting Response
               -> RadRaw Response
whenAccounting handler = do
  rTy <- gview _packetType
  if rTy == AccountingRequest
    then coerce handler
    else pure Skip

both :: Rad 'Access Response
     -> Rad 'Accounting Response
     -> RadRaw Response
both access accounting = do
  rTy <- gview _packetType
  case rTy of
    AccessRequest     -> coerce access
    AccountingRequest -> coerce accounting
    _                 -> pure Skip
