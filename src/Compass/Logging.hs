{-# LANGUAGE OverloadedStrings #-}

module Compass.Logging
  ( logWhat
  , logPacket
  , logDurationMs
  )
where

import Data.Int (Int64)

import Compass.Env
import Compass.Handler
import Compass.Types
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Logger (logDebugN, logInfoN)
import Network.RADIUS.Lens
import Network.RADIUS.Types

import Data.Text.Lazy (toStrict)
import System.Clock (Clock(..), TimeSpec(..), getTime)
import Text.Pretty.Simple

import Data.Text (Text, pack)
import Optics

showT :: Show a => a -> Text
showT = pack . show

(<+>) :: Text -> Text -> Text
f <+> g = f <> " " <> g

-- | Instrument a Response handler to log the in/out radius packets.
logPacket :: Rad k Response -> Rad k Response
logPacket act = do
    inp <- gview _requestPacket
    logDebugN (pprInPak inp)
    r <- act
    case r of
      Skip{}     -> pure ()
      Discard{}  -> pure ()
      Answer out -> logDebugN (pprOutPak out)

    pure r
  where
    pprInPak :: Packet -> Text
    pprInPak pak = toStrict (pShow pak)

    pprOutPak :: Packet -> Text
    pprOutPak pak = toStrict (pShow pak)


-- | Instrument a Response handler to log the duration it takes until completion.
logDurationMs :: Rad k Response -> Rad k Response
logDurationMs act = do
    start <- liftIO (getTime Monotonic)
    r <- act
    end <- liftIO (getTime Monotonic)
    logInfoN (pprTime start end)
    pure r
  where
    pprTime :: TimeSpec -> TimeSpec -> Text
    pprTime start end = "Completed in:" <+> showT (toMillis (end - start)) <> "ms"

    toMillis :: TimeSpec -> Int64
    toMillis (TimeSpec secs nanos) = secs * 1000 + (nanos `div` (1000 * 1000))

-- | Instrument a Response handler to log what requests come in, and how
-- what the response is.
logWhat :: Rad k Response -> Rad k Response
logWhat act = do
    ty <- gview _packetType
    cli <- gview _requestClient
    user <- (gview . pre) (_packetAttributes % folded % _AttrUserName)
    logInfoN (pprReq ty user cli)
    res <- act
    logInfoN (pprResp res user)

    pure res
  where

    -- Pretty print a request message for logging
    pprReq :: PacketType -> Maybe Text -> Client -> Text
    pprReq ty us cli = "Received:" <+> pprReqTy ty <+> pprSuppl us <+> pprClient cli

    -- Pretty print a response message for logging
    pprResp :: Response -> Maybe Text -> Text
    pprResp res us = "Sending:" <+> pprAnsTy res <+> pprSuppl us

    -- Pretty print the supplicant
    pprSuppl :: Maybe Text -> Text
    pprSuppl Nothing   = "for (anonymous user)"
    pprSuppl (Just us) = "for" <+> us

    pprAnsTy :: Response -> Text
    pprAnsTy Skip          = "Discard (no handlers left)"
    pprAnsTy (Discard why) = "Discard" <+> parens why
    pprAnsTy (Answer pak)  = showT (packetType pak)

    pprClient :: Client -> Text
    pprClient cli = "by" <+> showT (clientAddr cli) <+> parens (clientName cli)

    parens :: Text -> Text
    parens what = "(" <> what <> ")"

    pprReqTy :: PacketType -> Text
    pprReqTy = showT
